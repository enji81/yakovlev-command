import { Tank } from '../src/UObjects/Tank';
import { POSITION_ANGLE_KEY, VELOCITY_ROTATE_KEY, RotatebleAdapter } from '../src/Adapters/RotatebleAdepter';
import { RotateCommand } from '../src/Commands/RotateCommand';
describe('Проверка поворота', () => {
  const rotatebleObj = new Tank({
      [POSITION_ANGLE_KEY]: 360,
      [VELOCITY_ROTATE_KEY]: 90
  })

  const rotateAdapter = new RotatebleAdapter(rotatebleObj)
  const rotateCommand = new RotateCommand(rotateAdapter)

  it('Позиция угла поворота обьекта изменилась', () => {
    const result = 90
    rotateCommand.execute()
    expect(rotateAdapter.getRotateAngle()).toBe(result)
  })
})