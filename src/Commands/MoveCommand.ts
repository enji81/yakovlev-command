import { ICommand } from '../interfaces/ICommand';
import { MovebleAdapter } from '../Adapters/MovebleAdapter';

export class MoveCommand implements ICommand {
  private adapter: MovebleAdapter;
  constructor(adapter: MovebleAdapter) {
    this.adapter = adapter;
  }
  execute() {
    move(this.adapter)
  } 
}

function move(adapter: MovebleAdapter): void {
  const result = {
    x: adapter.getPosition().x + adapter.getVelocity().x,
    y: adapter.getPosition().y + adapter.getVelocity().y
  }
  adapter.setPosition(result)
}