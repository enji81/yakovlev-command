import { ICommand } from '../interfaces/ICommand';
import { ChangeAngleVelosityAdaptor } from '../Adapters/ChangeAngleVelosityAdaptor';

export class ChangeAngleVelosityCommand implements ICommand {
  private adapter: ChangeAngleVelosityAdaptor;
  constructor(adapter: ChangeAngleVelosityAdaptor) {
    this.adapter = adapter
  }

  execute() {
    changeAngleVelosity(this.adapter)
  }
}

function changeAngleVelosity(adapter: ChangeAngleVelosityAdaptor) {
const FULL_ANGLE = 360 
 
const result = {
   x: Math.round(Math.cos(adapter.getRotateAngle())*adapter.getFactorOfSpeed()),
   y: Math.round(Math.sin(adapter.getRotateAngle())*adapter.getFactorOfSpeed())
 }

 const newAngle = (adapter.getRotateAngle() + adapter.getRotateVelosity()) % FULL_ANGLE
 adapter.setVelocity(result)
 adapter.setRotateAngle(newAngle)
}