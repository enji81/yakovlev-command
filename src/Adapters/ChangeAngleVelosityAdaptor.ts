import { UObjectAdapter } from './UObjectAdapter';
import { Vector } from '../interfaces/IMovable';
import { VELOCITY_KEY } from './MovebleAdapter';
import { IChangebleVelosityAngle } from '../interfaces/IChangebleVelosityAngle';
import { VELOCITY_ROTATE_KEY, POSITION_ANGLE_KEY } from './RotatebleAdepter';

export const FACTOR_OF_SPEED_KEY = 'factor-of-speed'

export class ChangeAngleVelosityAdaptor extends UObjectAdapter implements IChangebleVelosityAngle
{
  setVelocity(newVelosity: Vector): void {
    this.UObject.setProperty(VELOCITY_KEY, newVelosity);
  }

  getVelosity(): Vector {
    return this.UObject.getProperty(VELOCITY_KEY);
  }

  getFactorOfSpeed(): number {
    return this.UObject.getProperty(FACTOR_OF_SPEED_KEY);
  }

  getRotateAngle(): number {
    return this.UObject.getProperty(POSITION_ANGLE_KEY)
  }

  setRotateAngle(newValue: number): void {
    this.UObject.setProperty(POSITION_ANGLE_KEY, newValue)
  }

  getRotateVelosity(): number {
    return this.UObject.getProperty(VELOCITY_ROTATE_KEY);
  }
}
